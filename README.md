Dependencies: Racket distribution,  
http://download.racket-lang.org  
Racket dependencies: threading.  
To install this package type in console  
```raco pkg install threading```  
To run it, Windows might be require to add path to   
racket distribution in $PATH environment variable.  
  
How to start:  
```racket <script-path> <args>```  
Linux example:  
```
racket ~/script/circles.rkt  \  
--awesome closure            \  
--set-wallpaper              \  
--num           10000        \  
--pic-size      1600 1200    \  
--cm            0.7          \  
--dot-radius    1            \  
--circle-radius 39 0         \  
--ground        black        \  
--circle        30 1 1 0.075 \  
--circle-border 0  0 0 0     \  
--dot           60 1 1 0.5   \  
--line          60 1 1 0.2   \  
--hue-shift     (shuf -i 0-360 -n 1)  
```  
Windows Example:  
```  
Racket.exe ~\script\circles.rkt `  
--set-wallpaper                 `  
--num           10000           `  
--pic-size      1600 1200       `  
--cm            0.6             `  
--dot-radius    1               `  
--circle-radius 13 65           `  
--ground        black           `  
--circle        0  1 1 0.01     `  
--circle-border 0  0 0 0        `  
--dot           60 1 1 0.5      `  
--line          60 1 1 0.25     `  
--hue-shift     (Get-Random -minimum 0 -maximum 360)
```
